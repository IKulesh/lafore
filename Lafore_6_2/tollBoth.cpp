#include "tollBoth.h"



tollBoth::tollBoth():cash_(0.0),cars_(0)
{
}

void tollBoth::payingCar()
{
	cars_++;
	cash_ += 0.5;
}

void tollBoth::nopayCar()
{
	cars_++;
}

void tollBoth::gisplay() const
{
	std::cout << "cars:\t" << std::setw(6) << cars_ << std::endl <<
		"cash:\t" <<std::setw(6) << '$' << cash_ << std::endl;
}


